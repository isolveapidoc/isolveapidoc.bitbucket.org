define({ "api": [
  {
    "type": "post",
    "url": "/api/v2/score/generation/:event_id",
    "title": "Generate scores",
    "version": "2.0.0",
    "name": "GenerateScores",
    "group": "Scoring",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "event_id",
            "description": "<p>id of the event</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "scores",
            "description": "<p>Event scores</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_company",
            "description": "<p>Score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_first_name",
            "description": "<p>Score delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_last_name",
            "description": "<p>Score delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.delegate_id",
            "description": "<p>Score delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "scores.score",
            "description": "<p>Score</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.sup_company",
            "description": "<p>Score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.supplier_id",
            "description": "<p>Score supplier id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "scoreboosts",
            "description": "<p>Event score boosts</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_company",
            "description": "<p>Score boost company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_first_name",
            "description": "<p>Score boost delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_last_name",
            "description": "<p>Score boost delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.delegate_id",
            "description": "<p>Score boost delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.pref_title",
            "description": "<p>Score boost parent name id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "scoreboosts.score",
            "description": "<p>Score boost score</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.sup_company",
            "description": "<p>Score boost company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.supplier_id",
            "description": "<p>Score boost supplier id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "filteredout",
            "description": "<p>Event scores filtered out</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_company",
            "description": "<p>Filtered score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_first_name",
            "description": "<p>Filtered score delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_last_name",
            "description": "<p>Filtered score delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.delegate_id",
            "description": "<p>Filtered score delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.pref_title",
            "description": "<p>Filtered score parent name id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.sup_company",
            "description": "<p>Filtered score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.supplier_id",
            "description": "<p>Filtered score supplier id</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"scores\": [{...}],\n    \"scoreboosts\": [{...}],\n    \"filteredout\": [{...}]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "EventNotFound",
            "description": "<p>The id of the Event does not exist</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "ISolvePreferenceNotFound",
            "description": "<p>Event has no scoring isolvepreferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "SuppliersNotFound",
            "description": "<p>Event has no suppliers</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "DelegatesNotFound",
            "description": "<p>Event has no delegates</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "SupplierPreferencesNotFound",
            "description": "<p>Event has no supplier preferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "DelegatePreferencesNotFound",
            "description": "<p>Event has no delegate preferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "NoScoresGenerated",
            "description": "<p>Event failed to generate scores</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"error\": \"EventNotFound\",\n    \"message\": \"The id of the Event does not exist\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Not Found\n{\n    \"error\": \"SystemError\",\n    \"message\": \"A system error message\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules/scoring/controllers/ScoreController.js",
    "groupTitle": "Scoring"
  },
  {
    "type": "post",
    "url": "/api/v2/score/generation/:event_id",
    "title": "Generate scores",
    "version": "2.0.0",
    "name": "GenerateScores",
    "group": "Scoring",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "event_id",
            "description": "<p>id of the event</p> "
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "scores",
            "description": "<p>Event scores</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_company",
            "description": "<p>Score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_first_name",
            "description": "<p>Score delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.del_last_name",
            "description": "<p>Score delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.delegate_id",
            "description": "<p>Score delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "scores.score",
            "description": "<p>Score</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.sup_company",
            "description": "<p>Score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scores.supplier_id",
            "description": "<p>Score supplier id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "scoreboosts",
            "description": "<p>Event score boosts</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_company",
            "description": "<p>Score boost company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_first_name",
            "description": "<p>Score boost delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.del_last_name",
            "description": "<p>Score boost delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.delegate_id",
            "description": "<p>Score boost delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.pref_title",
            "description": "<p>Score boost parent name id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "scoreboosts.score",
            "description": "<p>Score boost score</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.sup_company",
            "description": "<p>Score boost company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "scoreboosts.supplier_id",
            "description": "<p>Score boost supplier id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>Object[]</p> ",
            "optional": false,
            "field": "filteredout",
            "description": "<p>Event scores filtered out</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_company",
            "description": "<p>Filtered score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_first_name",
            "description": "<p>Filtered score delegate first name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.del_last_name",
            "description": "<p>Filtered score delegate last name</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.delegate_id",
            "description": "<p>Filtered score delegate id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.pref_title",
            "description": "<p>Filtered score parent name id</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.sup_company",
            "description": "<p>Filtered score company</p> "
          },
          {
            "group": "Success 200",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filteredout.supplier_id",
            "description": "<p>Filtered score supplier id</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"scores\": [{...}],\n    \"scoreboosts\": [{...}],\n    \"filteredout\": [{...}]         \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "EventNotFound",
            "description": "<p>The id of the Event does not exist</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "ISolvePreferenceNotFound",
            "description": "<p>Event has no scoring isolvepreferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "SuppliersNotFound",
            "description": "<p>Event has no suppliers</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "DelegatesNotFound",
            "description": "<p>Event has no delegates</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "SupplierPreferencesNotFound",
            "description": "<p>Event has no supplier preferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "DelegatePreferencesNotFound",
            "description": "<p>Event has no delegate preferences</p> "
          },
          {
            "group": "404",
            "optional": false,
            "field": "NoScoresGenerated",
            "description": "<p>Event failed to generate scores</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"error\": \"EventNotFound\",\n    \"message\": \"The id of the Event does not exist\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Not Found\n{\n    \"error\": \"SystemError\",\n    \"message\": \"A system error message\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "modules/scoring/controllers/ScoreController.ts",
    "groupTitle": "Scoring"
  }
] });